package br.com.redhat.configuration;

import org.apache.camel.component.kafka.KafkaConfiguration;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfigurations {

	@Value("${kafka.brokers}")
	private String brokers;

	@Bean(name = "railwaySpeedConfiguration")
	public KafkaConfiguration railwaySpeedConfiguration() {
		KafkaConfiguration configuration = this.generalConfiguration();
		configuration.setGroupId("CamelRailwaySpeed2");
		configuration.setClientId("RailwaySpeedSensor2");
		configuration.setTopic("dpf-topic");
		configuration.setConsumersCount(3);
		
        return configuration;
	}
	
	private KafkaConfiguration generalConfiguration() {
		KafkaConfiguration configuration = new KafkaConfiguration();
		configuration.setBrokers(brokers);
		configuration.setKeyDeserializer(StringDeserializer.class.getName());
		configuration.setValueDeserializer(StringDeserializer.class.getName());
		
        return configuration;
	}
}
