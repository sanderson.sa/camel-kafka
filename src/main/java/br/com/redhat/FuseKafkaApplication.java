package br.com.redhat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuseKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuseKafkaApplication.class, args);
	}

}
